const Message = require('./message.model');

const express = require('express');
const app = express();
const server = require('http').createServer(app);
const io = require('socket.io')(server);
const db = require('./users');

io.on('connection', (socket) => {
    socket.on('login', (login) => {
        const currUser = db.users.find((user) => {
            return user.name == login
        });
        socket.emit('login', currUser);
    });
    socket.on('message', (msg) => {
        db.users.forEach((item) => {
            const userMsg = new Message(msg.user ? msg.user : 'Anonymous', msg.message);
            item.messages.push(userMsg);
        });
        io.emit('message', msg)
    })
});

app.use(express.static('public'));

server.listen(3000, () => {
    console.log('node JS listening');
});