class message {
    constructor (sender, message) {
        this.sender = sender;
        this.message = message;
    }
}

module.exports = message;